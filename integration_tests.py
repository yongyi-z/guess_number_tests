import unittest
import io
from unittest import mock

from guess_number import *

class Scores_Game(unittest.TestCase):

    #testing random_int and high_low_win
    def test_random_guess_win(self):
        sc = Scores()

        num = random_int(0)
        captured = io.StringIO()
        sys.stdout = captured
        r = high_low_win(0, num, sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(r, 2)
        self.assertLessEqual(num, 0)
        self.assertEqual(captured.getvalue(), "You guessed right!! You won!!\n")
        self.assertEqual(sc.wins, 1)

    def test_random_guess_low(self):
        sc = Scores()

        num = random_int(1000000)
        captured = io.StringIO()
        sys.stdout = captured
        high_low_win(-1, num, sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(captured.getvalue(), "Guess is too low\n")
        self.assertLessEqual(num, 1000000)

    def test_random_guess_high(self):
        sc = Scores()

        num = random_int(10)
        captured = io.StringIO()
        sys.stdout = captured
        high_low_win(11, num, sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(captured.getvalue(), "Guess is too high\n")
        self.assertLessEqual(num, 10)


class Game_Game(unittest.TestCase):
    #test lost and end
    def test_lost_end(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        i = lost(sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(captured.getvalue(), "Oh no! You lost this round :(\n")
        self.assertEqual(sc.losts, 1)
        self.assertEqual(i, 2)

        if i == 2:
            input = mock.builtins.input
            mock.builtins.input = lambda _: "n"
            captured = io.StringIO()
            sys.stdout = captured
            with self.assertRaises(SystemExit) as cm:
                end(sc)
            sys.stdout = sys.__stdout__
            self.assertEqual(captured.getvalue(), "Your Wins:  0\nYour Losts:  1\n")
            self.assertEqual(cm.exception.code, "Game End")
            mock.builtins.input = input
        
    def test_random_win_end(self):   
        sc = Scores()

        num = random_int(0)
        captured = io.StringIO()
        sys.stdout = captured
        r = high_low_win(0, num, sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(r, 2)
        self.assertLessEqual(num, 0)
        self.assertEqual(captured.getvalue(), "You guessed right!! You won!!\n")
        self.assertEqual(sc.wins, 1)

        if r == 2:
            input = mock.builtins.input
            mock.builtins.input = lambda _: "n"
            captured = io.StringIO()
            sys.stdout = captured
            with self.assertRaises(SystemExit) as cm:
                end(sc)
            sys.stdout = sys.__stdout__
            self.assertEqual(captured.getvalue(), "Your Wins:  1\nYour Losts:  0\n")
            self.assertEqual(cm.exception.code, "Game End")
            mock.builtins.input = input



if __name__ == '__main__':
    unittest.main()