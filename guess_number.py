import random
import sys

class Scores:
    def __init__(self):
        self.wins = 0
        self.losts = 0

    def increase_wins(self):
        self.wins += 1

    def increase_losts(self):
        self.losts += 1

    def display(self):
        print("Your Wins: ", self.wins)
        print("Your Losts: ", self.losts)

def random_int(limit):
    num = random.randint(0, limit)
    return num

def high_low_win(guess, num, sc):
    if guess > num:
        print("Guess is too high")
    elif guess < num:
        print("Guess is too low")
    elif guess == num:
        print("You guessed right!! You won!!")
        sc.increase_wins()
        return 2

def lost(sc):
    print("Oh no! You lost this round :(")
    sc.increase_losts()
    return 2

def end(sc):
    choice = input("Play again[y/n]? ")

    if choice == 'y':
        main()
    elif choice == 'n':
        sc.display()
        sys.exit("Game End")

def difficulty():
    d = input("Choose your difficulty level: Easy  Normal  Difficult - ")

    if d == 'Easy':
        return 20

    elif d == 'Normal':
        return 10

    elif d == 'Difficult':
        return 5

    else:
        sys.exit("Did not enter one of three options")


def main():
    limit = int(input("Enter the Upper Limit: "))
    num = random_int(limit)
    diff = difficulty()

    c = str(diff)
    print("You have " + c +  " chances to guess right!")

    for x in range(diff):
        guess = int(input("Your Guess: "))
        i = high_low_win(guess, num, sc)
        if i == 2:
            end(sc)

    b = lost(sc)
    if b == 2:
        end(sc)


if __name__ == "__main__":
    sc = Scores()
    main()

