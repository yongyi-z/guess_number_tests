import unittest
import io
import sys
from unittest import mock

from guess_number import *


class ScoresTests(unittest.TestCase):
    #test scores class
    def test_increase_wins(self):
        sc = Scores()
        sc.increase_wins()
        self.assertEqual(sc.wins, 1)

    def test_increase_losts(self):
        sc = Scores()
        sc.increase_losts()
        self.assertEqual(sc.losts, 1)

    def test_initial_scores(self):
        sc = Scores()
        self.assertEqual(sc.wins, 0)
        self.assertEqual(sc.losts, 0)

    def test_display(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        sc.display()
        sys.stdout = sys.__stdout__
        
        self.assertEqual(captured.getvalue(), "Your Wins:  0\nYour Losts:  0\n")

class GameTest(unittest.TestCase):
    #test functions not of a class
    def test_rand_int(self):
        num = random_int(50)
        self.assertLessEqual(num, 50)

    def test_high(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        high_low_win(40, 20, sc)
        sys.stdout = sys.__stdout__

        self.assertEqual(captured.getvalue(), "Guess is too high\n")

    def test_low(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        high_low_win(2, 20, sc)
        sys.stdout = sys.__stdout__
        
        self.assertEqual(captured.getvalue(), "Guess is too low\n")

    def test_win(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        i = high_low_win(20, 20, sc)
        sys.stdout = sys.__stdout__
        
        self.assertEqual(captured.getvalue(), "You guessed right!! You won!!\n")
        self.assertEqual(sc.wins, 1)
        self.assertEqual(i, 2)

    def test_lost(self):
        sc = Scores()

        captured = io.StringIO()
        sys.stdout = captured
        i = lost(sc)
        sys.stdout = sys.__stdout__
        
        self.assertEqual(captured.getvalue(), "Oh no! You lost this round :(\n")
        self.assertEqual(sc.losts, 1)
        self.assertEqual(i, 2)

    def test_difficult_easy(self):
        input = mock.builtins.input
        mock.builtins.input = lambda _: "Easy"
        self.assertEqual(difficulty(), 20)
        mock.builtins.input = input

    def test_difficult_normal(self):
        input = mock.builtins.input
        mock.builtins.input = lambda _: "Normal"
        self.assertEqual(difficulty(), 10)
        mock.builtins.input = input

    def test_difficult_difficult(self):
        input = mock.builtins.input
        mock.builtins.input = lambda _: "Difficult"
        self.assertEqual(difficulty(), 5)
        mock.builtins.input = input

    def test_difficult_edge(self):
        input = mock.builtins.input
        mock.builtins.input = lambda _: "dhfhf"
        with self.assertRaises(SystemExit) as cm:
            difficulty()
        self.assertEqual(cm.exception.code, "Did not enter one of three options")
        mock.builtins.input = input

    def test_end(self):
        sc = Scores()

        input = mock.builtins.input
        mock.builtins.input = lambda _: "n"
        captured = io.StringIO()
        sys.stdout = captured
        with self.assertRaises(SystemExit) as cm:
            end(sc)
        sys.stdout = sys.__stdout__
        self.assertEqual(captured.getvalue(), "Your Wins:  0\nYour Losts:  0\n")
        self.assertEqual(cm.exception.code, "Game End")
        mock.builtins.input = input



if __name__ == '__main__':
    unittest.main()